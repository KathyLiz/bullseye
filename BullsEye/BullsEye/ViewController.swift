//
//  ViewController.swift
//  BullsEye
//
//  Created by Katherine Hurtado on 13/11/17.
//  Copyright © 2017 Katherine Hurtado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var labelMeta: UILabel!
    @IBOutlet weak var labelPuntaje: UILabel!
    @IBOutlet weak var labelRonda: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    //MARK:- Actions
    @IBAction func buttonRestart(_ sender: Any) {
    }
    
    @IBAction func buttonJugar(_ sender: Any) {
    }
    
    
    //MARK:- ViewControllerLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

